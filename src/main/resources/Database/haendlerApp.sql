drop database if exists haendlerApp;
create Database haendlerApp;
use haendlerApp;

create table haendler (
haendler_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
vorname varchar(50),
nachname Varchar(50));

create table auto (
auto_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
marke VARCHAR(30),
preis double,
km int,
haendler_id int,
foreign key (haendler_id) REFERENCES haendler(haendler_id)
);

insert into haendler values (1, "peter", "penis");
insert into haendler values (2, "vera", "vagina");
insert into haendler values (3, "kurt", "kizler");

insert into auto values (1, "ferrari", 200000.00, 1, 1);
insert into auto values (2, "ferrari", 300000.00, 134, 1);
insert into auto values (3, "ferrari", 400000.00, 423, 1);
insert into auto values (4, "ferrari", 500000.00, 561, 2);
insert into auto values (5, "ferrari", 500000.00, 561, 3);

select haendler_id, vorname, nachname, auto_id, marke, preis, km from haendler
join auto using (haendler_id);
