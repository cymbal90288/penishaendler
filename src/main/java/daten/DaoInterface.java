package daten;

import javafx.collections.ObservableList;
import logik.Haendler;

/**
 * Created by b7hafnb on 05.07.2016.
 */
public interface DaoInterface {

    public ObservableList<Haendler> getHaendlerData();


}
