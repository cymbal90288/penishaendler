package daten;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import logik.Auto;
import logik.Haendler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by b7hafnb on 05.07.2016.
 */
public class Database implements DaoInterface {


    public ObservableList<Haendler> getHaendlerData() {
        ObservableList<Haendler> haendlers = FXCollections.observableArrayList();
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "select haendler_id, vorname, nachname, auto_id, marke, preis, km from haendler join auto using (haendler_id)");) {
            ResultSet rs = statement.executeQuery();
            Map<Integer , Haendler> haenderById = new HashMap<>();

            while (rs.next()){
                int haendlerId = rs.getInt(1);
                String vorname = rs.getString(2);
                String nachname = rs.getString(3);


                Haendler haendler = haenderById.get(haendlerId);
                if(haendler == null) {
                    haendler = new Haendler(haendlerId, vorname, nachname);
                    haenderById.put(haendlerId, haendler);
                }
                haendler.setAutos(new Auto(rs.getInt(4), rs.getString(5), rs.getDouble(6), rs.getInt(7)));
            }
            haendlers.addAll(haenderById.values());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return haendlers;
    }

}
