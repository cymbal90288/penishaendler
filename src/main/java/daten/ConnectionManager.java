package daten;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by b7hafnb on 06.07.2016.
 */
public class ConnectionManager {
    private static ConnectionManager instance = new ConnectionManager();
    private static final String DB_URL = "jdbc:mysql://localhost:3306/haendlerApp";
    private static final String USER = "root";
    private static final String PASS = "root";


    private ConnectionManager(){

    }
    private Connection createConnection(){
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static Connection getConnection () {
        return instance.createConnection();
    }
}
