package logik;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import daten.*;

/**
 * Created by Michael.Faller on 28.06.2016.
 */
public class AutoController {

    @FXML
    private TableView<Auto> autoTable;
    @FXML
    private TableColumn<Auto,Integer> autoID;
    @FXML
    private TableColumn<Auto,String> autoMarke;
    @FXML
    private TableColumn<Auto,Double> autoPreis;
    @FXML
    private TableColumn<Auto,Integer> autoKM;

    @FXML
    private TableView<Haendler> haendlerTable;
    @FXML
    private TableColumn<Haendler,Integer> haendlerID;
    @FXML
    private TableColumn<Haendler,String> haendlerVorname;
    @FXML
    private TableColumn<Haendler,String > haendlerNachname;

    private DaoInterface daten = new Database();

    @FXML
    private void initialize() {
        haendlerID.setCellValueFactory(new PropertyValueFactory<Haendler, Integer>("haendlerID"));
        haendlerVorname.setCellValueFactory(cellData -> cellData.getValue().getHaendlerVornameProperty());
        haendlerNachname.setCellValueFactory(cellData -> cellData.getValue().getHaendlerNachnameProperty());


        haendlerTable.setItems(daten.getHaendlerData());
        haendlerTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showAllAutos(newValue));
    }

    public void showAllAutos(Haendler haendler){

        if(haendler != null) {
            autoID.setCellValueFactory(new PropertyValueFactory<Auto, Integer>("autoId"));
            autoMarke.setCellValueFactory(cellData -> cellData.getValue().autoMarkeProperty());
            autoPreis.setCellValueFactory(new PropertyValueFactory<Auto, Double>("autoPreis"));
            autoKM.setCellValueFactory(new PropertyValueFactory<Auto, Integer>("autoKM"));

            ObservableList<Auto> autoData = FXCollections.observableArrayList();
            autoData.addAll(haendler.getAutos());
            autoTable.setItems(autoData);
        }
    }

}
