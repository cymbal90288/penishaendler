package logik;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Michael.Faller on 28.06.2016.
 */
public class MainApp extends Application {



    private ObservableList<Auto> autoData = FXCollections.observableArrayList();

    public MainApp(){


    }



    private AnchorPane rootLayout;
    private Stage primaryStage;


    public static void main(String[] args) {
        launch(args);
    }

    public void openAPP() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/view/rootLayout.fxml"));
            rootLayout = (AnchorPane) loader.load();

            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();

        }
    }


    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        openAPP();



    }
}
