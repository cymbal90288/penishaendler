package logik;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michael.Faller on 28.06.2016.
 */
public class Haendler {


    private final IntegerProperty haendlerID;
    private final StringProperty haendlerVorname;
    private final StringProperty haendlerNachname;
    private List<Auto> autos = new ArrayList<>();

    public Haendler(int haendlerId, String haendlerVorname, String haendlerNachname) {
        this.haendlerID = new SimpleIntegerProperty(haendlerId);
        this.haendlerVorname = new SimpleStringProperty(haendlerVorname);
        this.haendlerNachname = new SimpleStringProperty(haendlerNachname);
    }

    public void setAutos (Auto auto){
        autos.add(auto);
    }

    public List<Auto> getAutos (){
        return autos;
    }

    public IntegerProperty haendlerIDProperty() {
        return haendlerID;
    }

    public StringProperty getHaendlerVornameProperty() {
        return haendlerVorname;
    }

    public StringProperty getHaendlerNachnameProperty() {
        return haendlerNachname;
    }


}
