package logik;


import javafx.beans.property.*;

/**
 * Created by Michael.Faller on 28.06.2016.
 */
public class Auto {

    private final IntegerProperty autoID;
    private final StringProperty autoMarke;
    private final DoubleProperty autoPreis;
    private final IntegerProperty autoKM;


    public Auto(int autoID, String autoMarke, Double preis, int km) {
        this.autoID = new SimpleIntegerProperty(autoID);
        this.autoMarke = new SimpleStringProperty(autoMarke);
        this.autoPreis = new SimpleDoubleProperty(preis);
        this.autoKM = new SimpleIntegerProperty(km);
    }

    public String getAutoMarke() {
        return autoMarke.get();
    }

    public StringProperty autoMarkeProperty() {
        return autoMarke;
    }

    public int getAutoId() {
        return autoID.get();
    }

    public IntegerProperty autoIdProperty() {
        return autoID;
    }

    public Double getAutoPreis() {
        return autoPreis.get();
    }
    public DoubleProperty autoPreisProperty() {
        return autoPreis;
    }

    public int getAutoKM() {
        return autoKM.get();
    }

    public IntegerProperty autoKMProperty() {
        return autoKM;
    }
}
